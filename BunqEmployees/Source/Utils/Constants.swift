//
//  Constants.swift
//  BunqEmployees
//
//  Created by Javier Servate on 19/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

struct Constants {
    static let cellIdentifier = "Cell"
    
    static let kEmployeeDirectoryDidUpdateNotification = "kEmployeeDirectoryDidUpdateNotification"
    
    static let profileImagesArray = [Asset.profile1.name, Asset.profile2.name, Asset.profile3.name, Asset.profile4.name, Asset.profile5.name]
    
    struct UserDefaults {
        static let lastEmployeeListFilterUsed = "lastEmployeeListFilterUsed"
    }
}
