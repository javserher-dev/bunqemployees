//
//  EmployeeDirectory.swift
//  BunqEmployees
//
//  Created by Javier Servate on 19/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class EmployeeDirectory: NSObject {
    let kEmployeeDirectoryDidUpdateNotification = "kEmployeeDirectoryDidUpdateNotification"
    private var isUpdating = false
    var employees: [Employee]?
    
    func update() {
        if isUpdating {
            return
        }
        isUpdating = true
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.BA_doUpdateInBackground()
        }
    }
    
    private func BA_doUpdateInBackground() {
        
        let name = ["Anne", "Lucas", "Marc", "Zeus", "Hermes", "Bart", "Paul", "John",
        "Ringo", "Dave", "Taylor"]
        let surnames = ["Hawkins", "Simpson", "Lennon", "Grohl", "Hawkins", "Jacobs",
        "Holmes", "Mercury", "Matthews"]
        let amount = name.count * surnames.count
        
        var employees = [Employee]()
        employees.reserveCapacity(amount)
        for employee in 0...amount {
            let fullName = String.init(format: "%@ %@", name[Int.random(in: 0...name.count-1)], surnames[Int.random(in: 0...surnames.count-1)])
            employees.append(Employee(with: fullName, birthYear: 1997 - Int.random(in: 0...50)))
        }
        
        DispatchQueue.main.async {
            self.BA_updateDidFinishWithResults(results: employees)
        }
    }
    
    private func BA_updateDidFinishWithResults(results: [Employee]) {
        employees = results
        isUpdating = false
        NotificationCenter.default.post(name: Notification.Name(kEmployeeDirectoryDidUpdateNotification), object: self)
    }
}
