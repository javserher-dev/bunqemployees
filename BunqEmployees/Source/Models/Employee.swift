//
//  Employee.swift
//  BunqEmployees
//
//  Created by Javier Servate on 19/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class Employee: NSObject {
    let kStartingSalary = Double(10000)
    let salaryCurrency = "EUR"
    var name: String
    var birthYear: Int
    var salary: Double
    
    init(with name: String, birthYear: Int) {
        self.name = name
        self.birthYear = birthYear
        self.salary = kStartingSalary
        super.init()
    }
    
    func formatSalaryToString() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = salaryCurrency
        guard let salaryString = formatter.string(from: NSNumber(value: salary)) else { return "" }
        return salaryString
    }
}
