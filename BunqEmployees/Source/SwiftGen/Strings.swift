// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum Buttons {
    internal enum EmployeeList {
      /// Filter A-Z 
      internal static let ascendantFilter = L10n.tr("Localizable", "Buttons.EmployeeList.AscendantFilter")
      /// Filter Z-A 
      internal static let descendantFilter = L10n.tr("Localizable", "Buttons.EmployeeList.DescendantFilter")
    }
  }

  internal enum TabBar {
    /// Directory
    internal static let firstItem = L10n.tr("Localizable", "TabBar.firstItem")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
