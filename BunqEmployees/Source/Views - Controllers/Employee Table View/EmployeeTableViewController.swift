//
//  EmployeeTableViewController.swift
//  BunqEmployees
//
//  Created by Javier Servate on 19/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import Reusable

final class EmployeeTableViewController: UITableViewController {
    
    private let employeeDirectory = EmployeeDirectory()
    private var isAscendantFiltering = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        isAscendantFiltering = UserDefaults.standard.bool(forKey: Constants.UserDefaults.lastEmployeeListFilterUsed)
        
        employeeDirectory.update()
        setupNavigationBar()
        observeNotification()
        setupTableView()
    }
    
    private func setupNavigationBar() {
        var rightNavigationButton: UIBarButtonItem
        if isAscendantFiltering {
            rightNavigationButton = UIBarButtonItem(title: L10n.Buttons.EmployeeList.ascendantFilter, style: .plain, target: self, action: #selector(filterEmployeeList))
        } else {
            rightNavigationButton = UIBarButtonItem(title: L10n.Buttons.EmployeeList.descendantFilter, style: .plain, target: self, action: #selector(filterEmployeeList))
        }
        
        navigationItem.rightBarButtonItem = rightNavigationButton
    }
    
    private func observeNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(didUpdateEmployeeDirectory), name: Notification.Name(Constants.kEmployeeDirectoryDidUpdateNotification), object: nil)
    }
    
    private func setupTableView() {
        tableView.register(cellType: EmployeeTableViewCell.self)
    }
    
    @objc func didUpdateEmployeeDirectory() {
        presentedViewController?.dismiss(animated: true, completion: nil)
        tableView.reloadData()
    }
    
    @objc func filterEmployeeList() {
        UserDefaults.standard.set(isAscendantFiltering, forKey: Constants.UserDefaults.lastEmployeeListFilterUsed)
        DispatchQueue.global(qos: .background).async { [weak self] in
            if let isAscendantFiltering = self?.isAscendantFiltering, isAscendantFiltering {
                self?.isAscendantFiltering = false
                self?.employeeDirectory.employees?.sort(by: { return $0.name < $1.name })
                self?.navigationItem.rightBarButtonItem?.title = L10n.Buttons.EmployeeList.descendantFilter
            } else {
                self?.isAscendantFiltering = true
                self?.employeeDirectory.employees?.sort(by: { return $0.name > $1.name })
                self?.navigationItem.rightBarButtonItem?.title = L10n.Buttons.EmployeeList.ascendantFilter
            }
            
            //The sorting can be done in the background thread but reload must be done in the main thread, otherwise the user won't be able to see the list sorted until he scrolls
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = employeeDirectory.employees?.count else { return 0 }
        return count
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 170
        } else {
            return 120
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: EmployeeTableViewCell.self)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? EmployeeTableViewCell, let employees = employeeDirectory.employees else { return }
        
        let salary = employees[indexPath.row].formatSalaryToString()
        for view in cell.contentView.subviews {
            view.removeFromSuperview()
        }
        
        if indexPath.row == 0 {
            cell.setupLayout(isBoss: true)
        } else {
            cell.setupLayout(isBoss: false)
        }
        cell.randomProfileImage = (indexPath.row + 5) % Constants.profileImagesArray.count
        cell.setContent(name: employees[indexPath.row].name, birthyear: employees[indexPath.row].birthYear, salary: salary)
    }
}
