//
//  EmployeeTableViewCell.swift
//  BunqEmployees
//
//  Created by Javier Servate on 19/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import Reusable

final class EmployeeTableViewCell: UITableViewCell, Reusable {
    
    private var nameLabel = UILabel()
    private var profileImageView = UIImageView()
    private var stackView = UIStackView()
    private var birthInfoView = DetailsInfoView()
    private var salaryInfoView = DetailsInfoView()
    var randomProfileImage = 0

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupLayout(isBoss: Bool) {
        
        stackView.addArrangedSubview(birthInfoView)
        stackView.addArrangedSubview(salaryInfoView)
        
        contentView.addSubview(nameLabel)
        contentView.addSubview(profileImageView)
        contentView.addSubview(stackView)
        
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        birthInfoView.translatesAutoresizingMaskIntoConstraints = false
        salaryInfoView.translatesAutoresizingMaskIntoConstraints = false
        
        if isBoss {
            setupLayoutForBoss()
        } else {
            setupLayoutForEmployee()
        }
        
        birthInfoView.setupLayout()
        salaryInfoView.setupLayout()
        
        setStyle(isBoss: isBoss)
    }
    
    private func setupLayoutForEmployee() {
        
        contentView.addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1, constant: 20))
        contentView.addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40))
        contentView.addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40))
        
        contentView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .centerY, relatedBy: .equal, toItem: profileImageView, attribute: .centerY, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .leadingMargin, relatedBy: .equal, toItem: profileImageView, attribute: .trailing, multiplier: 1, constant: 20))
        contentView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 135))
        
        contentView.addConstraint(NSLayoutConstraint(item: stackView, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 5))
        contentView.addConstraint(NSLayoutConstraint(item: stackView, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: 5))
        contentView.addConstraint(NSLayoutConstraint(item: stackView, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1, constant: 10))
        contentView.addConstraint(NSLayoutConstraint(item: stackView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 135))
    }
    
    private func setupLayoutForBoss() {
        contentView.addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .centerX, relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 20))
        contentView.addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60))
        contentView.addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60))
        
        contentView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .centerX, relatedBy: .equal, toItem: profileImageView, attribute: .centerX, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 150))
         contentView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40))
        contentView.addConstraint(NSLayoutConstraint(item: nameLabel, attribute: .topMargin, relatedBy: .equal, toItem: profileImageView, attribute: .bottom, multiplier: 1, constant: 10))
        
        contentView.addConstraint(NSLayoutConstraint(item: stackView, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: 0))
        contentView.addConstraint(NSLayoutConstraint(item: stackView, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1, constant: 5))
        contentView.addConstraint(NSLayoutConstraint(item: stackView, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1, constant: 5))
        contentView.addConstraint(NSLayoutConstraint(item: stackView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60))
    }
    
    private func setStyle(isBoss: Bool) {
        stackView.distribution = .fillEqually
        if isBoss {
           stackView.axis = .horizontal
        } else {
            stackView.axis = .vertical
        }
        
        nameLabel.textAlignment = .center
        nameLabel.numberOfLines = 2
    }
    
    func setContent(name: String, birthyear: Int, salary: String) {
        nameLabel.text = name
        profileImageView.image = UIImage(named: Constants.profileImagesArray[randomProfileImage])
        birthInfoView.setContent(info: String(birthyear), image: UIImage(named: "birthdayCake")!)
        salaryInfoView.setContent(info: salary, image: UIImage(named: "salary")!)
    }

}
