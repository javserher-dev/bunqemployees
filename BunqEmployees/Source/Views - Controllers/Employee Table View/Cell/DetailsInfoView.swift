//
//  BirthdayInfoView.swift
//  BunqEmployees
//
//  Created by Javier Servate on 20/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

class DetailsInfoView: UIView {

    private var infoLabel = UILabel()
    private var infoImageView = UIImageView()
    private var stackView = UIStackView()
    
    func setupLayout() {
        stackView.addArrangedSubview(infoImageView)
        stackView.addArrangedSubview(infoLabel)
        addSubview(stackView)
        
        infoLabel.clearsContextBeforeDrawing = true
        infoImageView.clearsContextBeforeDrawing = true
        self.clearsContextBeforeDrawing = true
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        infoImageView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: stackView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: stackView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: stackView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: stackView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: infoImageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: infoImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30))
        addConstraint(NSLayoutConstraint(item: infoImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30))
        
        addConstraint(NSLayoutConstraint(item: infoLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: infoLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 90))
        addConstraint(NSLayoutConstraint(item: infoLabel, attribute: .topMargin, relatedBy: .equal, toItem: infoImageView, attribute: .bottom, multiplier: 1, constant: 5))
        
        setStyle()
    }
    
    func setStyle() {
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        
        infoLabel.textAlignment = .center
        
    }
    
    func setContent(info: String, image: UIImage) {
        infoLabel.text = info
        infoImageView.image = image
    }
}
