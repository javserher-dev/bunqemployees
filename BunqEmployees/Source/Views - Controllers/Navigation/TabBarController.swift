//
//  TabBarController.swift
//  BunqEmployees
//
//  Created by Javier Servate on 19/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewControllers()
        setStyle()
    }
    
    private func setupViewControllers() {
        let first = UINavigationController(rootViewController: EmployeeTableViewController())
        first.tabBarItem = UITabBarItem(title: L10n.TabBar.firstItem, image: Asset.directory.image, selectedImage: Asset.directory.image)

        viewControllers = [first]
    }
    
    private func setStyle() {
        tabBar.tintColor = .blue
    }
}
