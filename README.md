# BunqEmployees

BuqEmployess just shows a directory of employees where the whole design was built programmatically to prove my skills (unlike the other projects where I used XIB�s), and that can be sorted by name (it is done in a background thread).

**Designed, implemented and structured always following the SOLID principles to make it as much reusable, scalable and easy to maintain as possible.**